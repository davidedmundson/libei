/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "config.h"

#include <errno.h>
#include <getopt.h>
#include <stdbool.h>

#include "util-io.h"
#include "util-mem.h"
#include "util-logger.h"
#include "util-strings.h"

#include <systemd/sd-bus.h>

struct portal {
	struct logger *logger;
	char *busname;
} portal;


#define call(_call) do { \
	int _rc = _call; \
	if (_rc < 0) { \
		log_error(portal, "Failed with %s %s:%d\n", strerror(-_rc), __func__, __LINE__); \
		return _rc; \
	} } while(0)

static int
request_close(sd_bus_message *m, void *userdata, sd_bus_error *ret_error)
{
	/* We don't live long enough for this to be called */
	return 0;
}

static const sd_bus_vtable request_vtable[] = {
	SD_BUS_VTABLE_START(0),
	SD_BUS_METHOD("Close", "", "", request_close, SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_SIGNAL("Response", "ua{sv}", 0),
	SD_BUS_VTABLE_END,
};

static int
create_request_object(struct portal *portal,
		      sd_bus *bus,
		      const char *objectpath)
{
	_unref_(sd_bus_slot) *slot = NULL;
	call(sd_bus_add_object_vtable(bus, &slot,
				      objectpath,
				      "org.freedesktop.portal.Request",
				      request_vtable,
				      NULL));

	int response = 0;
	log_debug(portal, "emitting Response %d on %s\n", response, objectpath);
	return sd_bus_emit_signal(bus,
			   objectpath,
			   "org.freedesktop.portal.Request",
			   "Response",
			   "ua{sv}",
			   response,
			   0 /* Array size */
			  );
	/* note: _unref_ removes object immediately */
}

static char *
sender_token(const char *input)
{
	if (!input)
		return NULL;

	char *token = strstrip(input, ":");
	for (size_t idx = 0; token[idx]; idx++) {
		if (token[idx] == '.')
			token[idx] = '_';
	}

	return token;
}

static int
portal_emulate_input(sd_bus_message *m, void *userdata,
		     sd_bus_error *ret_error)
{
	struct portal *portal = userdata;

	call(sd_bus_message_enter_container(m, 'a', "{sv}"));

	const char *key, *handle_token;
	call(sd_bus_message_read(m, "{sv}", &key, "s", &handle_token));
	/* we only have a handle token in the dict so far */
	if (!streq(key, "handle_token"))
		return -EINVAL;
	call(sd_bus_message_exit_container(m));

	_cleanup_free_ char *sender = sender_token(sd_bus_message_get_sender(m));
	if (!sender)
		return -ENOMEM;

	/* Send back the object path of the object we're about to create. We
	 * then create the object, so if that fails we have a problem but
	 * meh, this is for testing only .*/
	_cleanup_free_ char *objpath = xaprintf("%s/request/%s/%s",
						"/org/freedesktop/portal/desktop",
						sender,
						handle_token);
	call(sd_bus_reply_method_return(m, "o", objpath));

	/* now create the object */
	return create_request_object(portal, sd_bus_message_get_bus(m), objpath);
}

static int
portal_connect(sd_bus_message *m, void *userdata,
	       sd_bus_error *ret_error)
{
	struct portal *portal = userdata;

	const char *xdg = getenv("XDG_RUNTIME_DIR");
	if (!xdg)
		return -ENOENT;

	_cleanup_free_ char *sockpath = xaprintf("%s/eis-0", xdg);
	int handle = xconnect(sockpath);
	log_debug(portal, "passing Handle %d\n", handle);
	return sd_bus_reply_method_return(m, "h", handle);
}

static const sd_bus_vtable portal_vtable[] = {
	SD_BUS_VTABLE_START(0),
	SD_BUS_METHOD("EmulateInput", "a{sv}", "o", portal_emulate_input, SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_METHOD("Connect", "a{sv}", "h", portal_connect, SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_VTABLE_END,
};

static int
run(struct portal *portal)
{
	_unref_(sd_bus) *bus = NULL;
	_unref_(sd_bus_slot) *slot = NULL;
	int rc = sd_bus_open_user(&bus);
	if (rc < 0)
		return rc;

	rc = sd_bus_add_object_vtable(bus, &slot,
				      "/org/freedesktop/portal/desktop",
				      "org.freedesktop.portal.EmulatedInput",
				      portal_vtable,
				      portal);
	if (rc < 0)
		return rc;

	log_debug(portal, "Portal object at: /org/freedesktop/portal/desktop\n");

	rc = sd_bus_request_name(bus, portal->busname, 0);
	if (rc < 0)
		return rc;

	log_debug(portal, "Portal DBus name: %s\n", portal->busname);

	_unref_(sd_event) *event = NULL;
	rc = sd_event_default(&event);
	if (rc < 0)
		return rc;

	rc = sd_event_set_watchdog(event, true);
	if (rc < 0)
		return rc;

	rc = sd_bus_attach_event(bus, event, 0);
	if (rc < 0)
		return rc;

	return sd_event_loop(event);
}

static void
usage(FILE *fp, const char *argv0)
{
	fprintf(fp,
		"Usage: %s [--busname=a.b.c.d]\n"
		"\n"
		"Emulates an XDG Desktop portal for the org.freedesktop.portal.EmulatedInput interface\n"
		"\n"
		"Options:\n"
		" --busname	    use the given busname instead of the default org.freedesktop.libei.Desktop\n"
		"",
		basename(argv0));
}

int
main(int argc, char **argv)
{
	_cleanup_free_ char *busname = xstrdup("org.freedesktop.portal.Desktop");

	while (1) {
		enum opts {
			OPT_BUSNAME,
		};
		static struct option long_opts[] = {
			{ "busname",	required_argument, 0, OPT_BUSNAME},
			{ "help",	no_argument, 0, 'h'},
			{ NULL},
		};

		int optind = 0;
		int c = getopt_long(argc, argv, "h", long_opts, &optind);
		if (c == -1)
			break;

		switch(c) {
			case 'h':
				usage(stdout, argv[0]);
				return EXIT_SUCCESS;
			case OPT_BUSNAME:
				free(busname);
				busname = xstrdup(optarg);
				break;
			default:
				usage(stderr, argv[0]);
				return EXIT_FAILURE;
		}
	}

	portal.busname = steal(&busname);
	portal.logger = logger_new("portal", NULL);
	logger_set_priority(portal.logger, LOGGER_DEBUG);

	int rc = run(&portal);
	if (rc < 0)
		fprintf(stderr, "Failed to start fake portal: %s\n", strerror(-rc));

	logger_unref(portal.logger);
	free(portal.busname);
	return rc == 0;
}
