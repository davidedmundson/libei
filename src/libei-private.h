/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <stdarg.h>

#include "util-macros.h"
#include "util-object.h"

#include "libei.h"
#include "util-list.h"
#include "util-sources.h"
#include "util-structs.h"

struct ei_backend_interface {
	void (*destroy)(struct ei *ei, void *backend);
};

enum ei_state {
	EI_STATE_NEW,		/* No backend yet */
	EI_STATE_BACKEND,	/* We have a backend */
	EI_STATE_CONNECTING,	/* client requested connect */
	EI_STATE_CONNECTED,	/* server has sent connect */
	EI_STATE_DISCONNECTING, /* in the process of cleaning up */
	EI_STATE_DISCONNECTED,
};

struct ei {
	struct object object;
	void *user_data;
	struct sink *sink;
	struct source *source;
	struct ei_backend_interface backend_interface;
	void *backend;
	enum ei_state state;
	struct list event_queue;
	struct list seats;
	char *name;

	struct {
		ei_log_handler handler;
		enum ei_log_priority priority;
	} log;
};

enum ei_seat_state {
	EI_SEAT_STATE_PRESENT,
	EI_SEAT_STATE_REMOVED,
};

struct ei_seat {
	struct object object;
	void *user_data;
	struct list link;
	enum ei_seat_state state;
	/* devices created by client but not yet added */
	struct list devices_pending;
	struct list devices;
	uint32_t id;
	uint32_t capabilities;
	char *name;
};

enum ei_device_state {
	EI_DEVICE_STATE_NEW,
	EI_DEVICE_STATE_CONNECTING,
	EI_DEVICE_STATE_SUSPENDED,
	EI_DEVICE_STATE_RESUMED,
	/**
	 * Client removed the device, we no longer accept events from the
	 * client
	 */
	EI_DEVICE_STATE_REMOVED_FROM_CLIENT,
	/**
	 * Server removed the device, we need to remove it ourselves now.
	 */
	EI_DEVICE_STATE_REMOVED_FROM_SERVER,
	/**
	 * Device has been removed by both sides.
	 */
	EI_DEVICE_STATE_DEAD,
};

struct ei_device {
	struct object object;
	void *user_data;
	struct list link;
	uint32_t id;
	enum ei_device_state state;
	uint32_t capabilities;
	char *name;

	struct  {
		struct dimensions dim;
	} abs;

	struct  {
		struct dimensions dim;
	} touch;

	struct ei_keymap *keymap;
};

struct ei_keymap {
	struct object object;
	struct ei_device *device;
	enum ei_keymap_source source;
	enum ei_keymap_type type;
	int fd;
	size_t size;
	bool assigned;
};

struct ei_touch {
	struct object object;
	struct ei_device *device;
	void *user_data;
	uint32_t tracking_id;
	enum {
		TOUCH_IS_NEW,
		TOUCH_IS_DOWN,
		TOUCH_IS_UP,
	} state;

	double x, y;
};

struct ei_event {
	struct object object;
	enum ei_event_type type;
	struct list link;
	struct ei_client *client;
	struct ei_seat *seat; /* NULL if device is non-NULL */
	struct ei_device *device;
};

int
ei_set_connection(struct ei *ei, int fd);

void
ei_disconnect(struct ei *ei);

struct ei_seat *
ei_seat_new(struct ei *ei, uint32_t id, const char *name,
	    uint32_t capabilities);

struct ei_device *
ei_seat_find_device(struct ei_seat *seat, uint32_t deviceid);

void
ei_seat_remove(struct ei_seat *seat);

void
ei_seat_drop(struct ei_seat *seat);

int
ei_send_add_device(struct ei_device *device);

int
ei_send_remove_device(struct ei_device *device);

void
ei_queue_device_removed_event(struct ei_device *device);

void
ei_insert_device_removed_event(struct ei_device *device);

void
ei_queue_seat_removed_event(struct ei_seat *seat);

void
ei_device_removed_by_server(struct ei_device *device);

int
ei_send_pointer_rel(struct ei_device *device,
		    double x, double y);

int
ei_send_pointer_abs(struct ei_device *device,
		    double x, double y);

int
ei_send_pointer_button(struct ei_device *device,
		       uint32_t button, bool is_press);

int
ei_send_keyboard_key(struct ei_device *device,
		     uint32_t key, bool is_press);

int
ei_send_touch_down(struct ei_device *device, uint32_t tid,
		    double x, double y);
int
ei_send_touch_motion(struct ei_device *device, uint32_t tid,
		     double x, double y);
int
ei_send_touch_up(struct ei_device *device, uint32_t tid);

void
ei_device_added(struct ei_device *device);

void
ei_device_suspended(struct ei_device *device);

void
ei_device_resumed(struct ei_device *device);

void
ei_device_set_name(struct ei_device *device, const char *name);

void
ei_device_set_seat(struct ei_device *device, const char *seat);

void
ei_device_set_capabilities(struct ei_device *device,
			   uint32_t capabilities);

void
ei_device_set_keymap(struct ei_device *device,
		     enum ei_keymap_type type,
		     int keymap_fd,
		     size_t size);

_printf_(3, 4) void
ei_log_msg(struct ei *ei,
	   enum ei_log_priority priority,
	   const char *format, ...);

_printf_(3, 0) void
ei_log_msg_va(struct ei *ei,
	      enum ei_log_priority priority,
	      const char *format,
	      va_list args);

#define log_debug(T_, ...) \
	ei_log_msg((T_), EI_LOG_PRIORITY_DEBUG, __VA_ARGS__)
#define log_info(T_, ...) \
	ei_log_msg((T_), EI_LOG_PRIORITY_INFO, __VA_ARGS__)
#define log_warn(T_, ...) \
	ei_log_msg((T_), EI_LOG_PRIORITY_WARNING, __VA_ARGS__)
#define log_error(T_, ...) \
	ei_log_msg((T_), EI_LOG_PRIORITY_ERROR, __VA_ARGS__)
#define log_bug(T_, ...) \
	ei_log_msg((T_), EI_LOG_PRIORITY_ERROR, "🪳  libei bug:  " __VA_ARGS__)
#define log_bug_client(T_, ...) \
	ei_log_msg((T_), EI_LOG_PRIORITY_ERROR, "🪲  Bug: " __VA_ARGS__)
