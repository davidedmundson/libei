/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "config.h"

#include <errno.h>

#include "util-macros.h"
#include "util-bits.h"
#include "util-io.h"

#include "libeis-private.h"

_public_
OBJECT_IMPLEMENT_REF(eis_keymap);
_public_
OBJECT_IMPLEMENT_UNREF(eis_keymap);
_public_
OBJECT_IMPLEMENT_GETTER(eis_keymap, type, enum eis_keymap_type);
_public_
OBJECT_IMPLEMENT_GETTER(eis_keymap, fd, int);
_public_
OBJECT_IMPLEMENT_GETTER(eis_keymap, size, size_t);

static void
eis_keymap_destroy(struct eis_keymap *keymap)
{
	xclose(keymap->fd);
}

static
OBJECT_IMPLEMENT_CREATE(eis_keymap);

_public_ struct eis_keymap *
eis_keymap_new(enum eis_keymap_type type, int fd, size_t size)
{
	_unref_(eis_keymap) *keymap = eis_keymap_create(NULL);

	switch (type) {
	case EIS_KEYMAP_TYPE_XKB:
		break;
	default:
		return NULL;
	}

	if (fd < 0 || size == 0)
		return NULL;

	int newfd = dup(fd);
	if (newfd < 0)
		return NULL;

	keymap->fd = newfd;
	keymap->type = type;
	keymap->size = size;

	return eis_keymap_ref(keymap);
}

static inline struct eis *
eis_device_get_context(struct eis_device *device)
{
	return eis_client_get_context(eis_device_get_client(device));
}

_public_ void
eis_device_keyboard_set_keymap(struct eis_device *device,
			       struct eis_keymap *keymap)
{
	if (device->state != EIS_DEVICE_STATE_NEW) {
		log_bug_client(eis_device_get_context(device),
			       "%s: device already (dis)connected\n", __func__);
		return;
	}

	if (device->keymap && !device->keymap->is_client_keymap) {
		log_bug_client(eis_device_get_context(device),
			       "%s: keymap can only be configured once\n", __func__);
		return;
	}

	if (keymap && keymap->assigned) {
		log_bug_client(eis_device_get_context(device),
			       "%s: keymaps cannot be re-used\n", __func__);
		return;
	}

	device->keymap = eis_keymap_unref(device->keymap);
	if (keymap) {
		keymap->assigned = true;
		device->keymap = eis_keymap_ref(keymap);
	}
}

_public_ struct eis_keymap *
eis_device_keyboard_get_keymap(struct eis_device *device)
{
	return device->keymap;
}

static void
eis_device_destroy(struct eis_device *device)
{
	eis_keymap_unref(device->keymap);
	free(device->name);
}

_public_
OBJECT_IMPLEMENT_REF(eis_device);
_public_
OBJECT_IMPLEMENT_UNREF(eis_device);
static
OBJECT_IMPLEMENT_CREATE(eis_device);
static
OBJECT_IMPLEMENT_PARENT(eis_device, eis_seat);
_public_
OBJECT_IMPLEMENT_GETTER(eis_device, user_data, void *);
_public_
OBJECT_IMPLEMENT_SETTER(eis_device, user_data, void *);
_public_
OBJECT_IMPLEMENT_GETTER(eis_device, name, const char *);

_public_ struct eis_seat *
eis_device_get_seat(struct eis_device *device)
{
	return eis_device_parent(device);
}

_public_ void
eis_device_set_name(struct eis_device *device, const char *name)
{
	if (device->state != EIS_DEVICE_STATE_NEW) {
		log_bug_client(eis_device_get_context(device),
			       "%s: device already (dis)connected\n", __func__);
		return;
	}

	free(device->name);
	device->name = xstrdup(name);
}

_public_ struct eis_client *
eis_device_get_client(struct eis_device *device)
{
	return eis_seat_get_client(eis_device_get_seat(device));
}

struct eis_device *
eis_device_new(struct eis_seat *seat,
	       uint32_t id,
	       const char *name,
	       uint32_t capabilities)
{
	struct eis_device *device = eis_device_create(&seat->object);

	device->name = xstrdup(name);
	device->capabilities_mask = 0;
	device->capabilities = capabilities;
	device->id = id;
	device->state = EIS_DEVICE_STATE_NEW;

	return device;
}

void
eis_device_set_pointer_range(struct eis_device *device,
			     uint32_t w, uint32_t h)
{
	device->abs.dim.width = w;
	device->abs.dim.height = h;
}

void
eis_device_set_touch_range(struct eis_device *device,
			   uint32_t w, uint32_t h)
{
	device->touch.dim.width = w;
	device->touch.dim.height = h;
}

void
eis_device_set_client_keymap(struct eis_device *device,
			     enum eis_keymap_type type,
			     int keymap_fd, size_t size)
{
	if (device->state != EIS_DEVICE_STATE_NEW) {
		log_bug(eis_device_get_context(device),
			"%s: device already (dis)connected\n", __func__);
		return;
	}

	if (type == 0 && keymap_fd == -1)
		return;

	_unref_(eis_keymap) *keymap =
		eis_keymap_new(type, keymap_fd, size);
	if (!keymap) {
		log_bug(eis_device_get_context(device),
			"%s: keymap creation failed, using NULL keymap\n", __func__);
		return;
	}

	eis_device_keyboard_set_keymap(device, keymap);
	keymap->is_client_keymap = true;
	keymap->assigned = true;
}

_public_ bool
eis_device_has_capability(struct eis_device *device,
			  enum eis_device_capability cap)
{
	switch (cap) {
	case EIS_DEVICE_CAP_POINTER:
	case EIS_DEVICE_CAP_POINTER_ABSOLUTE:
	case EIS_DEVICE_CAP_KEYBOARD:
	case EIS_DEVICE_CAP_TOUCH:
		return flag_is_set(device->capabilities, cap);
	}
	return false;
}

_public_ void
eis_device_allow_capability(struct eis_device *device,
			    enum eis_device_capability cap)
{
	switch(cap) {
	case EIS_DEVICE_CAP_POINTER:
	case EIS_DEVICE_CAP_POINTER_ABSOLUTE:
	case EIS_DEVICE_CAP_KEYBOARD:
	case EIS_DEVICE_CAP_TOUCH:
		flag_set(device->capabilities_mask, cap);
		break;
	}
}

_public_ uint32_t
eis_device_pointer_get_width(struct eis_device *device)
{
	return device->abs.dim.width;
}

_public_ uint32_t
eis_device_pointer_get_height(struct eis_device *device)
{
	return device->abs.dim.height;
}

_public_ uint32_t
eis_device_touch_get_width(struct eis_device *device)
{
	return device->touch.dim.width;
}

_public_ uint32_t
eis_device_touch_get_height(struct eis_device *device)
{
	return device->touch.dim.height;
}

int
eis_device_pointer_rel(struct eis_device *device,
		       double x, double y)
{
	if (!eis_device_has_capability(device, EIS_DEVICE_CAP_POINTER)) {
		log_bug_client(eis_device_get_context(device),
			       "%s: device is not a pointer\n", __func__);
		return -EINVAL;
	}

	if (device->state != EIS_DEVICE_STATE_RESUMED)
		return -EINVAL;

	eis_queue_pointer_rel_event(device, x, y);

	return 0;
}

int
eis_device_pointer_abs(struct eis_device *device,
		       double x, double y)
{
	if (!eis_device_has_capability(device, EIS_DEVICE_CAP_POINTER_ABSOLUTE)) {
		log_bug_client(eis_device_get_context(device),
			       "%s: device is not an absolute pointer\n", __func__);
		return -EINVAL;
	}

	if (device->state != EIS_DEVICE_STATE_RESUMED)
		return -EINVAL;

	if (x < 0 || x >= device->abs.dim.width ||
	    y < 0 || y >= device->abs.dim.height)
		return -EINVAL;

	eis_queue_pointer_abs_event(device, x, y);

	return 0;
}

int
eis_device_pointer_button(struct eis_device *device,
		          uint32_t button, bool is_press)
{
	if (!eis_device_has_capability(device, EIS_DEVICE_CAP_POINTER)) {
		log_bug_client(eis_device_get_context(device),
			       "%s: device is not a pointer\n", __func__);
		return -EINVAL;
	}

	if (device->state != EIS_DEVICE_STATE_RESUMED)
		return -EINVAL;

	eis_queue_pointer_button_event(device, button, is_press);

	return 0;
}

int
eis_device_keyboard_key(struct eis_device *device,
			uint32_t key, bool is_press)
{
	if (!eis_device_has_capability(device, EIS_DEVICE_CAP_KEYBOARD)) {
		log_bug_client(eis_device_get_context(device),
			       "%s: device is not a keyboard\n", __func__);
		return -EINVAL;
	}

	if (device->state != EIS_DEVICE_STATE_RESUMED)
		return -EINVAL;

	eis_queue_keyboard_key_event(device, key, is_press);

	return 0;
}

int
eis_device_touch(struct eis_device *device, uint32_t touchid,
		 bool is_down, bool is_up,
		 double x, double y)
{
	if (!eis_device_has_capability(device, EIS_DEVICE_CAP_TOUCH)) {
		log_bug_client(eis_device_get_context(device),
			       "%s: device is not a touch device\n", __func__);
		return -EINVAL;
	}

	if (device->state != EIS_DEVICE_STATE_RESUMED)
		return -EINVAL;

	if (is_down)
		eis_queue_touch_down_event(device, touchid, x, y);
	else if (is_up)
		eis_queue_touch_up_event(device, touchid);
	else
		eis_queue_touch_motion_event(device, touchid, x, y);

	return 0;
}

_public_ void
eis_device_connect(struct eis_device *device)
{
	if (device->state != EIS_DEVICE_STATE_NEW)
		return;

	if (device->capabilities_mask == 0) {
		log_bug(eis_device_get_context(device),
			"Connecting a device without capabilities\n");
		eis_device_disconnect(device);
		return;
	}

	device->capabilities &= device->capabilities_mask;
	device->state = EIS_DEVICE_STATE_SUSPENDED;
	eis_client_connect_device(eis_device_get_client(device), device);
}

_public_ void
eis_device_disconnect(struct eis_device *device)
{
	switch (device->state) {
	case EIS_DEVICE_STATE_DEAD:
	case EIS_DEVICE_STATE_REMOVED_BY_SERVER:
		break;
	/* server is the first to drop the device. Notify the client but the
	 * local event comes when the client acks it */
	case EIS_DEVICE_STATE_NEW:
	case EIS_DEVICE_STATE_SUSPENDED:
	case EIS_DEVICE_STATE_RESUMED:
		device->state = EIS_DEVICE_STATE_REMOVED_BY_SERVER;
		eis_client_disconnect_device(eis_device_get_client(device), device);
		break;
	/* device was already removed by the client, so it's properly gone
	 * now */
	case EIS_DEVICE_STATE_REMOVED_BY_CLIENT:
		device->state = EIS_DEVICE_STATE_DEAD;
		list_remove(&device->link);
		eis_device_unref(device);
		break;
	}
}

void
eis_device_removed_by_client(struct eis_device *device)
{
	switch (device->state) {
	case EIS_DEVICE_STATE_DEAD:
	case EIS_DEVICE_STATE_REMOVED_BY_CLIENT:
		/* libei bug, ignore */
		break;
	/* Confirmation from the client. Queue our local event but otherwise
	 * this device is done */
	case EIS_DEVICE_STATE_REMOVED_BY_SERVER:
		eis_queue_removed_event(device);
		device->state = EIS_DEVICE_STATE_DEAD;
		list_remove(&device->link);
		eis_device_unref(device);
		break;
	case EIS_DEVICE_STATE_NEW:
	case EIS_DEVICE_STATE_SUSPENDED:
	case EIS_DEVICE_STATE_RESUMED:
		eis_queue_removed_event(device);
		device->state = EIS_DEVICE_STATE_REMOVED_BY_CLIENT;
		eis_client_disconnect_device(eis_device_get_client(device), device);
		break;
	}
}

_public_ void
eis_device_suspend(struct eis_device *device)
{
	if (device->state != EIS_DEVICE_STATE_RESUMED)
		return;

	device->state = EIS_DEVICE_STATE_SUSPENDED;
	eis_client_suspend_device(eis_device_get_client(device), device);
}

_public_ void
eis_device_resume(struct eis_device *device)
{
	if (device->state != EIS_DEVICE_STATE_SUSPENDED)
		return;

	device->state = EIS_DEVICE_STATE_RESUMED;
	eis_client_resume_device(eis_device_get_client(device), device);
}
