/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include <config.h>

#include "libreis.h"

#include "util-bits.h"
#include "util-object.h"
#include "util-macros.h"
#include "util-strings.h"
#include "util-mem.h"
#include "util-io.h"

#include "proto/ei.pb-c.h"

struct reis {
	struct object object;
	char *name;
	bool policy_is_allow;
	uint32_t allow;
	uint32_t deny;
};

static void
reis_destroy(struct reis *reis)
{
	free(reis->name);
}

_public_
OBJECT_IMPLEMENT_UNREF(reis);
static
OBJECT_IMPLEMENT_CREATE(reis);

static int
send_msg(int fd, const ClientMessage *msg)
{
	size_t msglen = client_message__get_packed_size(msg);
	Frame frame = FRAME__INIT;
	frame.length = msglen;
	size_t framelen = frame__get_packed_size(&frame);

	uint8_t buf[framelen + msglen];
	frame__pack(&frame, buf);
	client_message__pack(msg, buf + framelen);
	return min(0, xsend(fd, buf, sizeof(buf)));
}

_public_ struct reis *
reis_new(void)
{
	struct reis *reis = reis_create(NULL);

	reis->policy_is_allow = true;

	return reis;
}

_public_ int
reis_apply(struct reis *reis, int eisfd)
{
	if (reis->name) {
		ConfigureName n = CONFIGURE_NAME__INIT;
		n.name = reis->name;

		ClientMessage msg = CLIENT_MESSAGE__INIT;
		msg.configure_name = &n;
		msg.msg_case = CLIENT_MESSAGE__MSG_CONFIGURE_NAME;

		int rc = send_msg(eisfd, &msg);
		if (rc)
			return rc;
	}

	if (!reis->policy_is_allow || reis->allow || reis->deny) {
		ConfigureCapabilities c = CONFIGURE_CAPABILITIES__INIT;
		c.policy_is_allow = reis->policy_is_allow;
		c.allowed_capabilities = reis->allow;
		c.denied_capabilities = reis->deny;

		ClientMessage msg = CLIENT_MESSAGE__INIT;
		msg.configure_caps = &c;
		msg.msg_case = CLIENT_MESSAGE__MSG_CONFIGURE_CAPS;

		int rc = send_msg(eisfd, &msg);
		if (rc)
			return rc;
	}

	return 0;
}

_public_ int
reis_set_name(struct reis *reis, const char *name)
{
	free(reis->name);
	reis->name = xstrdup(name);

	return 0;
}

_public_ int
reis_set_cap_policy_allow(struct reis *reis)
{
	reis->policy_is_allow = true;
	return 0;
}

_public_ int
reis_set_cap_policy_deny(struct reis *reis)
{
	reis->policy_is_allow = false;
	return 0;
}

_public_ int
reis_allow_cap(struct reis *reis, enum eis_device_capability cap)
{
	switch (cap) {
	case EIS_DEVICE_CAP_POINTER:
	case EIS_DEVICE_CAP_POINTER_ABSOLUTE:
	case EIS_DEVICE_CAP_KEYBOARD:
	case EIS_DEVICE_CAP_TOUCH:
		reis->allow |= bit(cap);
		return 0;
	}
	return -EINVAL;
}

_public_ int
reis_deny_cap(struct reis *reis, enum eis_device_capability cap)
{
	switch (cap) {
	case EIS_DEVICE_CAP_POINTER:
	case EIS_DEVICE_CAP_POINTER_ABSOLUTE:
	case EIS_DEVICE_CAP_KEYBOARD:
	case EIS_DEVICE_CAP_TOUCH:
		reis->deny |= bit(cap);
		return 0;
	}
	return -EINVAL;
}
