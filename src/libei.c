/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "config.h"

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "util-io.h"
#include "util-macros.h"
#include "util-object.h"
#include "util-sources.h"
#include "util-strings.h"

#include "libei.h"
#include "libei-private.h"
#include "libei-proto.h"
#include "brei-shared.h"

static const char *
ei_event_type_to_string(enum ei_event_type type)
{
	switch(type) {
	CASE_RETURN_STRING(EI_EVENT_CONNECT);
	CASE_RETURN_STRING(EI_EVENT_DISCONNECT);
	CASE_RETURN_STRING(EI_EVENT_SEAT_ADDED);
	CASE_RETURN_STRING(EI_EVENT_SEAT_REMOVED);
	CASE_RETURN_STRING(EI_EVENT_DEVICE_ADDED);
	CASE_RETURN_STRING(EI_EVENT_DEVICE_REMOVED);
	CASE_RETURN_STRING(EI_EVENT_DEVICE_SUSPENDED);
	CASE_RETURN_STRING(EI_EVENT_DEVICE_RESUMED);
	}

	assert(!"Unhandled event type");
}

static void
ei_event_destroy(struct ei_event *event)
{
	switch (event->type) {
	case EI_EVENT_CONNECT:
	case EI_EVENT_DISCONNECT:
	case EI_EVENT_SEAT_ADDED:
	case EI_EVENT_SEAT_REMOVED:
	case EI_EVENT_DEVICE_ADDED:
	case EI_EVENT_DEVICE_REMOVED:
	case EI_EVENT_DEVICE_SUSPENDED:
	case EI_EVENT_DEVICE_RESUMED:
		break;
	default:
		assert(!"destroy not implemented for this type");
	}
	ei_device_unref(event->device);
	ei_seat_unref(event->seat);
}

static
OBJECT_IMPLEMENT_CREATE(ei_event);
static
OBJECT_IMPLEMENT_REF(ei_event);
_public_
OBJECT_IMPLEMENT_UNREF(ei_event);
_public_
OBJECT_IMPLEMENT_GETTER(ei_event, type, enum ei_event_type);
_public_
OBJECT_IMPLEMENT_GETTER(ei_event, device, struct ei_device*);

_public_ struct ei_seat *
ei_event_get_seat(struct ei_event *event)
{
	return event->device ? ei_device_get_seat(event->device) : event->seat;
}

static struct ei_seat *
ei_find_seat(struct ei *ei, uint32_t seatid)
{
	struct ei_seat *seat;

	list_for_each(seat, &ei->seats, link) {
		if (seat->id == seatid)
			return seat;
	}

	return NULL;
}

static struct ei_device *
ei_find_device(struct ei *ei, uint32_t deviceid)
{
	struct ei_seat *seat;

	list_for_each(seat, &ei->seats, link) {
		struct ei_device *device = ei_seat_find_device(seat, deviceid);
		if (device)
			return device;
	}

	return NULL;
}

static void
ei_destroy(struct ei *ei)
{
	ei_disconnect(ei);

	struct ei_event *e;
	while ((e = ei_get_event(ei)) != NULL)
		ei_event_unref(e);

	if (ei->backend_interface.destroy)
		ei->backend_interface.destroy(ei, ei->backend);
	ei->backend = NULL;
	sink_unref(ei->sink);
	free(ei->name);
}

static
OBJECT_IMPLEMENT_CREATE(ei);
_public_
OBJECT_IMPLEMENT_REF(ei);
_public_
OBJECT_IMPLEMENT_UNREF(ei);
_public_
OBJECT_IMPLEMENT_SETTER(ei, user_data, void *);
_public_
OBJECT_IMPLEMENT_GETTER(ei, user_data, void *);

_public_ struct ei *
ei_new(void *user_data)
{
	_unref_(ei) *ei = ei_create(NULL);

	ei->state = EI_STATE_NEW;
	list_init(&ei->event_queue);
	list_init(&ei->seats);

	ei_log_set_handler(ei, NULL);
	ei_log_set_priority(ei, EI_LOG_PRIORITY_INFO);
	ei->sink = sink_new();
	if (!ei->sink)
		return NULL;

	ei->user_data = user_data;
	ei->backend = NULL;

	return steal(&ei);
}

_public_ int
ei_get_fd(struct ei *ei)
{
	return sink_get_fd(ei->sink);
}

_public_ void
ei_dispatch(struct ei *ei)
{
	sink_dispatch(ei->sink);
}

static void
queue_event(struct ei *ei, struct ei_event *event)
{
	log_debug(ei, "queuing event type %s (%d)\n",
		  ei_event_type_to_string(event->type), event->type);

	list_append(&ei->event_queue, &event->link);
}

static void
insert_event(struct ei *ei, struct ei_event *event)
{
	log_debug(ei, "inserting event type %s (%d)\n",
		  ei_event_type_to_string(event->type), event->type);

	list_insert(&ei->event_queue, &event->link);
}


static void
queue_connect_event(struct ei *ei)
{
	struct ei_event *e = ei_event_create(&ei->object);
	e->type = EI_EVENT_CONNECT;

	queue_event(ei, e);
}

static void
queue_disconnect_event(struct ei *ei)
{
	struct ei_event *e = ei_event_create(&ei->object);
	e->type = EI_EVENT_DISCONNECT;

	queue_event(ei, e);
}

static void
queue_seat_added_event(struct ei_seat *seat)
{
	struct ei *ei= ei_seat_get_context(seat);

	struct ei_event *e = ei_event_create(&ei->object);
	e->type = EI_EVENT_SEAT_ADDED;
	e->seat = ei_seat_ref(seat);

	queue_event(ei, e);
}

static void
queue_seat_removed_event(struct ei_seat *seat)
{
	struct ei *ei= ei_seat_get_context(seat);

	struct ei_event *e = ei_event_create(&ei->object);
	e->type = EI_EVENT_SEAT_REMOVED;
	e->seat = ei_seat_ref(seat);

	queue_event(ei, e);
}

void
ei_queue_seat_removed_event(struct ei_seat *seat)
{
	queue_seat_removed_event(seat);
}

static void
queue_device_added_event(struct ei_device *device)
{
	struct ei *ei= ei_device_get_context(device);

	struct ei_event *e = ei_event_create(&ei->object);
	e->type = EI_EVENT_DEVICE_ADDED;
	e->device = ei_device_ref(device);

	queue_event(ei, e);
}

static void
queue_device_removed_event(struct ei_device *device)
{
	struct ei *ei= ei_device_get_context(device);

	struct ei_event *e = ei_event_create(&ei->object);
	e->type = EI_EVENT_DEVICE_REMOVED;
	e->device = ei_device_ref(device);

	queue_event(ei, e);
}

static void
insert_device_removed_event(struct ei_device *device)
{
	struct ei *ei= ei_device_get_context(device);

	struct ei_event *e = ei_event_create(&ei->object);
	e->type = EI_EVENT_DEVICE_REMOVED;
	e->device = ei_device_ref(device);

	insert_event(ei, e);
}

static void
queue_suspended_event(struct ei_device *device)
{
	struct ei *ei= ei_device_get_context(device);

	struct ei_event *e = ei_event_create(&ei->object);
	e->type = EI_EVENT_DEVICE_SUSPENDED;
	e->device = ei_device_ref(device);

	queue_event(ei, e);
}

static void
queue_resumed_event(struct ei_device *device)
{
	struct ei *ei= ei_device_get_context(device);

	struct ei_event *e = ei_event_create(&ei->object);
	e->type = EI_EVENT_DEVICE_RESUMED;
	e->device = ei_device_ref(device);

	queue_event(ei, e);
}

static int
connection_send_connect(struct ei *ei)
{
	if (ei->state == EI_STATE_NEW ||
	    ei->state == EI_STATE_DISCONNECTED)
		return 0;

	return ei_proto_send_connect(ei);
}

static int
connection_send_disconnect(struct ei *ei)
{
	if (ei->state == EI_STATE_NEW ||
	    ei->state == EI_STATE_DISCONNECTED)
		return 0;

	return ei_proto_send_disconnect(ei);
}

static int
connection_send_add(struct ei *ei, struct ei_device *device)
{
	if (ei->state == EI_STATE_NEW ||
	    ei->state == EI_STATE_DISCONNECTED)
		return 0;

	return ei_proto_send_add(ei, device);
}

static int
connection_send_remove(struct ei *ei, struct ei_device *device)
{
	if (ei->state == EI_STATE_NEW ||
	    ei->state == EI_STATE_DISCONNECTED)
		return 0;

	return ei_proto_send_remove(ei, device);
}

static int
connection_send_rel(struct ei *ei, struct ei_device *device,
		    double x, double y)
{
	if (ei->state == EI_STATE_NEW ||
	    ei->state == EI_STATE_DISCONNECTED)
		return 0;

	return ei_proto_send_rel(ei, device, x, y);
}

static int
connection_send_abs(struct ei *ei, struct ei_device *device,
		    double x, double y)
{
	if (ei->state == EI_STATE_NEW ||
	    ei->state == EI_STATE_DISCONNECTED)
		return 0;

	return ei_proto_send_abs(ei, device, x, y);
}

static int
connection_send_button(struct ei *ei, struct ei_device *device,
		       uint32_t b, bool is_press)
{
	if (ei->state == EI_STATE_NEW ||
	    ei->state == EI_STATE_DISCONNECTED)
		return 0;

	return ei_proto_send_button(ei, device, b, is_press);
}

static int
connection_send_key(struct ei *ei, struct ei_device *device,
		    uint32_t k, bool is_press)
{
	if (ei->state == EI_STATE_NEW ||
	    ei->state == EI_STATE_DISCONNECTED)
		return 0;

	return ei_proto_send_key(ei, device, k, is_press);
}

static int
connection_send_touch_down(struct ei *ei, struct ei_device *device,
			   uint32_t tid, double x, double y)
{
	if (ei->state == EI_STATE_NEW ||
	    ei->state == EI_STATE_DISCONNECTED)
		return 0;

	return ei_proto_send_touch_down(ei, device, tid, x, y);
}

static int
connection_send_touch_motion(struct ei *ei, struct ei_device *device,
			     uint32_t tid, double x, double y)
{
	if (ei->state == EI_STATE_NEW ||
	    ei->state == EI_STATE_DISCONNECTED)
		return 0;

	return ei_proto_send_touch_motion(ei, device, tid, x, y);
}

static int
connection_send_touch_up(struct ei *ei, struct ei_device *device, uint32_t tid)
{
	if (ei->state == EI_STATE_NEW ||
	    ei->state == EI_STATE_DISCONNECTED)
		return 0;

	return ei_proto_send_touch_up(ei, device, tid);
}

void
ei_disconnect(struct ei *ei)
{
	if (ei->state == EI_STATE_DISCONNECTED ||
	    ei->state == EI_STATE_DISCONNECTING)
		return;

	enum ei_state state = ei->state;

	/* We need the disconnecting state to be re-entrant
	   ei_device_remove() may call ei_disconnect() on a socket error */
	ei->state = EI_STATE_DISCONNECTING;

	struct ei_seat *seat, *tmps;
	list_for_each_safe(seat, tmps, &ei->seats, link) {
		ei_seat_remove(seat);
	}

	if (state != EI_STATE_NEW) {
		connection_send_disconnect(ei);
		queue_disconnect_event(ei);
	}
	ei->state = EI_STATE_DISCONNECTED;
	if (ei->source)
		source_remove(ei->source);
	ei->source = source_unref(ei->source);
}

int
ei_send_add_device(struct ei_device *device)
{
	struct ei *ei = ei_device_get_context(device);
	int rc = connection_send_add(ei, device);
	if (rc)
		ei_disconnect(ei);

	return rc;
}

int
ei_send_remove_device(struct ei_device *device)
{
	struct ei *ei = ei_device_get_context(device);
	int rc = connection_send_remove(ei, device);
	if (rc)
		ei_disconnect(ei);

	return rc;
}

static int
handle_msg_seat_added(struct ei *ei, uint32_t seatid,
		      const char *name, uint32_t capabilities)
{
	log_debug(ei, "Added seat %#x '%s' with caps %#x\n",
		  seatid, name, capabilities);

	struct ei_seat *seat = ei_seat_new(ei, seatid, name, capabilities);

	list_append(&ei->seats, &seat->link);

	queue_seat_added_event(seat);

	return 0;
}

static int
handle_msg_seat_removed(struct ei *ei, uint32_t seatid)
{
	log_debug(ei, "server removed seat %#x\n", seatid);

	struct ei_seat *seat = ei_find_seat(ei, seatid);
	if (seat) {
		ei_seat_remove(seat);
	}

	return 0;
}

static int
handle_msg_device_added(struct ei *ei, uint32_t deviceid,
			const char *name, uint32_t capabilities,
			bool keymap_from_server, enum ei_keymap_type keymap_type,
			int keymap_fd, size_t keymap_sz,
			uint32_t seatid)
{
	struct ei_seat *seat = ei_find_seat(ei, seatid);

	if (!seat) {
		log_bug(ei, "Invalid seat id %d for device %s (%d)\n",
			seatid, name, deviceid);
		return 0;
	}

	struct ei_device *device = ei_seat_find_device(seat, deviceid);

	/* Wrong device id or a device already removed by the client but we
	 * won't know which unless we keep some device ID table. Not worth
	 * it, so just silently ignore */
	if (!device)
		return 0;

	ei_device_set_name(device, name);
	ei_device_set_capabilities(device, capabilities);
	if (keymap_from_server)
		ei_device_set_keymap(device, keymap_type,
				     keymap_fd, keymap_sz);
	ei_device_added(device);

	log_debug(ei,
		  "Added device %#x '%s' caps: %s%s%s%s seat: %s\n",
		  deviceid, name,
		  ei_device_has_capability(device, EI_DEVICE_CAP_POINTER) ? "p" : "",
		  ei_device_has_capability(device, EI_DEVICE_CAP_POINTER_ABSOLUTE) ? "a" : "",
		  ei_device_has_capability(device, EI_DEVICE_CAP_KEYBOARD) ? "k" : "",
		  ei_device_has_capability(device, EI_DEVICE_CAP_TOUCH) ? "t" : "",
		  ei_seat_get_name(seat));

	queue_device_added_event(device);
	return 0;
}

void
ei_queue_device_removed_event(struct ei_device *device)
{
	queue_device_removed_event(device);
}

void
ei_insert_device_removed_event(struct ei_device *device)
{
	insert_device_removed_event(device);
}

static int
handle_msg_device_removed(struct ei *ei, uint32_t deviceid)
{
	log_debug(ei, "Removed device %#x\n", deviceid);

	struct ei_device *device = ei_find_device(ei, deviceid);
	if (!device)
		return 0;

	ei_device_removed_by_server(device);

	return 0;
}

static int
handle_msg_resumed(struct ei *ei, uint32_t deviceid)
{
	log_debug(ei, "Resumed device %#x\n", deviceid);

	struct ei_device *device = ei_find_device(ei, deviceid);
	if (device) {
		ei_device_resumed(device);
		queue_resumed_event(device);
	}

	return 0;
}

static int
handle_msg_suspended(struct ei *ei, uint32_t deviceid)
{
	log_debug(ei, "Suspended device %d\n", deviceid);

	struct ei_device *device = ei_find_device(ei, deviceid);
	if (device) {
		ei_device_suspended(device);
		queue_suspended_event(device);
	}

	return 0;
}

int
ei_send_pointer_rel(struct ei_device *device, double x, double y)
{
	struct ei *ei = ei_device_get_context(device);
	int rc = connection_send_rel(ei, device, x, y);
	if (rc)
		ei_disconnect(ei);
	return rc;
}

int
ei_send_pointer_abs(struct ei_device *device, double x, double y)
{
	struct ei *ei = ei_device_get_context(device);
	int rc = connection_send_abs(ei, device, x, y);
	if (rc)
		ei_disconnect(ei);
	return rc;
}

int
ei_send_pointer_button(struct ei_device *device, uint32_t button, bool is_press)
{
	struct ei *ei = ei_device_get_context(device);
	int rc = connection_send_button(ei, device, button, is_press);
	if (rc)
		ei_disconnect(ei);
	return rc;
}

int
ei_send_keyboard_key(struct ei_device *device, uint32_t key, bool is_press)
{
	struct ei *ei = ei_device_get_context(device);
	int rc = connection_send_key(ei, device, key, is_press);
	if (rc)
		ei_disconnect(ei);
	return rc;
}

int
ei_send_touch_down(struct ei_device *device, uint32_t tid,
		   double x, double y)
{
	struct ei *ei = ei_device_get_context(device);
	int rc = connection_send_touch_down(ei, device, tid, x, y);
	if (rc)
		ei_disconnect(ei);
	return rc;
}

int
ei_send_touch_motion(struct ei_device *device, uint32_t tid,
		     double x, double y)
{
	struct ei *ei = ei_device_get_context(device);
	int rc = connection_send_touch_motion(ei, device, tid, x, y);
	if (rc)
		ei_disconnect(ei);
	return rc;
}

int
ei_send_touch_up(struct ei_device *device, uint32_t tid)
{
	struct ei *ei = ei_device_get_context(device);
	int rc = connection_send_touch_up(ei, device, tid);
	if (rc)
		ei_disconnect(ei);
	return rc;
}

_public_ struct ei_event*
ei_get_event(struct ei *ei)
{
	if (list_empty(&ei->event_queue))
		return NULL;

	struct ei_event *e = list_first_entry(&ei->event_queue, e, link);
	list_remove(&e->link);

	return e;
}

_public_ struct ei_event*
ei_peek_event(struct ei *ei)
{
	if (list_empty(&ei->event_queue))
		return NULL;

	struct ei_event *e = list_first_entry(&ei->event_queue, e, link);
	return ei_event_ref(e);
}

static int
connection_new_handle_msg(struct ei *ei, struct message *msg)
{
	int rc = 0;

	switch (msg->type) {
	case MESSAGE_CONNECTED:
	case MESSAGE_DISCONNECTED:
	case MESSAGE_SEAT_ADDED:
	case MESSAGE_SEAT_REMOVED:
	case MESSAGE_DEVICE_ADDED:
	case MESSAGE_DEVICE_REMOVED:
	case MESSAGE_DEVICE_RESUMED:
	case MESSAGE_DEVICE_SUSPENDED:
		rc = -EPROTO;
		break;
	}

	return rc;
}

static int
connection_connecting_handle_msg(struct ei *ei, struct message *msg)
{
	int rc = 0;

	switch (msg->type) {
	case MESSAGE_CONNECTED:
		ei->state = EI_STATE_CONNECTED;
		queue_connect_event(ei);
		break;
	case MESSAGE_DISCONNECTED:
		rc = -ECANCELED;
		break;
	case MESSAGE_SEAT_ADDED:
	case MESSAGE_SEAT_REMOVED:
	case MESSAGE_DEVICE_ADDED:
	case MESSAGE_DEVICE_REMOVED:
	case MESSAGE_DEVICE_RESUMED:
	case MESSAGE_DEVICE_SUSPENDED:
		rc = -EPROTO;
		break;
	}

	return rc;
}

static int
connection_connected_handle_msg(struct ei *ei, struct message *msg)
{
	int rc = 0;

	switch (msg->type) {
	case MESSAGE_CONNECTED:
		rc = -EPROTO;
		break;
	case MESSAGE_DISCONNECTED:
		rc = -ECANCELED;
		break;
	case MESSAGE_SEAT_ADDED:
		rc = handle_msg_seat_added(ei,
					   msg->seat_added.seatid,
					   msg->seat_added.name,
					   msg->seat_added.capabilities);
		break;
	case MESSAGE_SEAT_REMOVED:
		rc = handle_msg_seat_removed(ei, msg->seat_removed.seatid);
		break;
	case MESSAGE_DEVICE_ADDED:
		rc = handle_msg_device_added(ei,
					     msg->device_added.deviceid,
					     msg->device_added.name,
					     msg->device_added.capabilities,
					     msg->device_added.keymap_from_server,
					     msg->device_added.keymap_type,
					     msg->device_added.keymap_fd,
					     msg->device_added.keymap_size,
					     msg->device_added.seatid);
		break;
	case MESSAGE_DEVICE_REMOVED:
		rc = handle_msg_device_removed(ei, msg->device_removed.deviceid);
		break;
	case MESSAGE_DEVICE_RESUMED:
		rc = handle_msg_resumed(ei, msg->resumed.deviceid);
		break;
	case MESSAGE_DEVICE_SUSPENDED:
		rc = handle_msg_suspended(ei, msg->resumed.deviceid);
		break;
	}

	return rc;
}

static int
connection_message_callback(struct brei_message *bmsg, void *userdata)
{
	struct ei *ei = userdata;
	size_t consumed;

	_cleanup_message_ struct message *msg = ei_proto_parse_message(bmsg, &consumed);
	if (!msg)
		return -EBADMSG;

	log_debug(ei, "handling message type %s\n", message_type_to_string(msg->type));

	int rc = 0;
	switch (ei->state) {
		case EI_STATE_NEW:
			abort();
		case EI_STATE_BACKEND:
			rc = connection_new_handle_msg(ei, msg);
			break;
		case EI_STATE_CONNECTING:
			rc = connection_connecting_handle_msg(ei, msg);
			break;
		case EI_STATE_CONNECTED:
			rc = connection_connected_handle_msg(ei, msg);
			break;
		case EI_STATE_DISCONNECTING:
		case EI_STATE_DISCONNECTED:
#if 0
			/* FIXME: this shouldn't happen, but right now we
			 * may end up calling ei_disconnect() on an error
			 * which cleans most things up, leaving us with the
			 * message processing once we have actually cleaned
			 * up. Needs a bigger rework than currently
			 * possible.
			 */
			assert(!"Protocol error: message received while disconnecting\n");
#endif
			break;
	}

	if (rc < 0)
		return rc;
	else
		return consumed;
}

static void
connection_dispatch(struct source *source, void *userdata)
{
	struct ei *ei = userdata;
	enum ei_state old_state = ei->state;

	int rc = brei_dispatch(source_get_fd(source), connection_message_callback, ei);
	if (rc < 0)
		ei_disconnect(ei);

	static const char *states[] = {
		"NEW",
		"BACKEND",
		"CONNECTING",
		"CONNECTED",
		"DISCONNECTED",
		"DISCONNECTING",
	};
	if (rc == -ECANCELED)
		log_info(ei, "Disconnected\n");
	else if (rc)
		log_warn(ei, "Connnection error: %s\n", strerror(-rc));

	if (old_state != ei->state)
		log_debug(ei, "Connnection dispatch: %s -> %s\n",
			  states[old_state],
			  states[ei->state]);
}

int
ei_set_connection(struct ei *ei, int fd)
{
	struct source *source = source_new(fd, connection_dispatch, ei);
	int rc = sink_add_source(ei->sink, source);
	if (rc == 0) {
		ei->source = source_ref(source);
		ei->state = EI_STATE_BACKEND;
		rc = connection_send_connect(ei);
		if (rc != 0) {
			log_error(ei, "message failed to send: %s\n", strerror(-rc));
			ei_disconnect(ei);
		} else {
			ei->state = EI_STATE_CONNECTING;
		}
	}

	source_unref(source);

	return rc;
}

_public_ void
ei_configure_name(struct ei *ei, const char *name)
{
	if (ei->state != EI_STATE_NEW) {
		log_bug_client(ei,"Client is already connected\n");
		return;
	}

	if (strlen(name) > 1024) {
		log_bug_client(ei, "Client name too long\n");
		return;
	}

	free(ei->name);
	ei->name = xstrdup(name);
}

#ifdef _enable_tests_
#include "util-munit.h"

MUNIT_TEST(test_init_unref)
{
	struct ei *ei = ei_new(NULL);

	munit_assert_int(ei->state, ==, EI_STATE_NEW);
	munit_assert(list_empty(&ei->event_queue));
	munit_assert(list_empty(&ei->seats));

	munit_assert_not_null(ei->sink);

	struct ei *refd = ei_ref(ei);
	munit_assert_ptr_equal(ei, refd);
	munit_assert_int(ei->object.refcount, ==, 2);

	struct ei *unrefd = ei_unref(ei);
	munit_assert_null(unrefd);

	unrefd = ei_unref(ei);
	munit_assert_null(unrefd);

	return MUNIT_OK;
}

MUNIT_TEST(test_configure_name)
{
	struct ei *ei = ei_new(NULL);

	ei_configure_name(ei, "foo");
	munit_assert_string_equal(ei->name, "foo");
	ei_configure_name(ei, "bar");
	munit_assert_string_equal(ei->name, "bar");

	/* ignore names that are too long */
	char buf[1200] = {0};
	memset(buf, 'a', sizeof(buf) - 1);
	ei_configure_name(ei, buf);
	munit_assert_string_equal(ei->name, "bar");

	/* ignore names in all other states */
	for (enum ei_state state = EI_STATE_NEW + 1;
	     state <= EI_STATE_DISCONNECTED;
	     state++) {
		ei->state = state;
		ei_configure_name(ei, "expect ignored");
		munit_assert_string_equal(ei->name, "bar");
	}

	ei_unref(ei);

	return MUNIT_OK;
}
#endif
