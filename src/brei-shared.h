/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include "config.h"

struct brei_message {
	const char *data;
	size_t len;
};

/**
 * Return the first file descriptor passed along with this message, or -1.
 * You must only call this when a message is supposed to have an fd.
 *
 * Brei can not determine which message an fd belongs to, hence calling this
 * function for a non-fd-carrying message will take that fd away from the
 * message that fd actually belongs to.
 */
int
brei_message_take_fd(struct brei_message *b);

/**
 * Dispatch messages for the data on fd.
 * The callback is called for each protocol message, parsing must be done by
 * the caller.
 *
 * On success, the callback must return the number of bytes consumed in msgdata
 * On failure, the callback must return a negative errno
 *
 * @return zero on success or a negative errno otherwise
 */
int
brei_dispatch(int fd,
	      int (*callback)(struct brei_message *msg, void *userdata),
	      void *user_data);
