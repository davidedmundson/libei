/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "config.h"

#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <systemd/sd-bus.h>

#include "libei.h"
#include "libei-private.h"

#include "util-io.h"
#include "util-macros.h"
#include "util-mem.h"
#include "util-object.h"
#include "util-strings.h"

struct ei_portal {
	struct object object;
	struct source *bus_source;
	sd_bus *bus;
	sd_bus_slot *slot;

	char *busname;
};

static void
ei_portal_destroy(struct ei_portal *portal)
{
	free(portal->busname);
	sd_bus_unref(portal->bus);
}

static
OBJECT_IMPLEMENT_CREATE(ei_portal);
static
OBJECT_IMPLEMENT_PARENT(ei_portal, ei);
static
OBJECT_IMPLEMENT_REF(ei_portal);
static
OBJECT_IMPLEMENT_UNREF(ei_portal);

static void
interface_portal_destroy(struct ei *ei, void *backend)
{
	struct ei_portal *portal = backend;
	ei_portal_unref(portal);
}

static const struct ei_backend_interface interface = {
	.destroy = interface_portal_destroy,
};

static char *
xdp_token(sd_bus *bus, char **token_out)
{
	_cleanup_free_ char *sender = NULL;
	const char *name = NULL;

	if (sd_bus_get_unique_name(bus, &name) != 0)
		return NULL;

	sender = xstrdup(name + 1); /* drop initial : */

	for (unsigned i = 0; sender[i]; i++) {
		if (sender[i] == '.')
			sender[i] = '_';
	}

	char *token = xaprintf("ei_%d", rand());
	*token_out = token;

	return xaprintf("/org/freedesktop/portal/desktop/request/%s/%s", sender, token);
}

static void
portal_connect(struct ei_portal *portal)
{
	_cleanup_(sd_bus_error_free) sd_bus_error error = SD_BUS_ERROR_NULL;
	_unref_(sd_bus_message) *response = NULL;
	struct sd_bus *bus = portal->bus;
	struct ei *ei = ei_portal_parent(portal);
	int eisfd;

	int rc = sd_bus_call_method(bus, portal->busname,
				    "/org/freedesktop/portal/desktop",
				    "org.freedesktop.portal.EmulatedInput",
				    "Connect",
				    &error,
				    &response,
				    "a{sv}", 0);

	if (rc < 0) {
		log_error(ei, "Failed to call method: %s\n", strerror(-rc));
		goto out;
	}

	rc = sd_bus_message_read(response, "h", &eisfd);
	if (rc < 0) {
		log_error(ei, "Failed to extract fd: %s\n", strerror(-rc));
		goto out;
	}

	/* the fd is owned by the message */
	rc = xerrno(dup(eisfd));
	if (rc < 0) {
		log_error(ei, "Failed to dup fd: %s\n", strerror(-rc));
		goto out;
	} else {
		eisfd = rc;
		int flags = fcntl(eisfd, F_GETFL, 0);
		fcntl(eisfd, F_SETFL, flags | O_NONBLOCK);
	}

	log_debug(ei, "Initiating ei context with fd %d from portal\n", eisfd);

	/* We're done with DBus, lets clean up */
	source_remove(portal->bus_source);
	source_unref(portal->bus_source);
	portal->bus = sd_bus_unref(portal->bus);

	rc = ei_set_connection(ei, eisfd);
out:
	if (rc < 0) {
		log_error(ei, "Failed to set the connection: %s\n", strerror(-rc));
		ei_disconnect(ei);
	}
}

static int
portal_response_received(sd_bus_message *m, void *userdata, sd_bus_error *error)
{
	struct ei_portal *portal = userdata;
	struct ei *ei = ei_portal_parent(portal);
	unsigned response;

	assert(m);
	assert(portal);

	/* We'll only get this signal once */
	portal->slot = sd_bus_slot_unref(portal->slot);

	int rc = sd_bus_message_read(m, "u",  &response);
	if (rc < 0) {
		log_error(ei, "Failed to read response from signal: %s\n", strerror(-rc));
		ei_disconnect(ei);
		return 0;
	}

	log_debug(ei, "Portal EmulateInput reponse is %d\n", response);

	if (response != 0) {
		ei_disconnect(ei);
		return 0;
	}

	portal_connect(portal);

	return 0;
}

static void
dbus_dispatch(struct source *source, void *data)
{
	struct ei_portal *portal = data;

	/* We need to ref the bus here, portal_connect() may remove
	 * portal->bus but that needs to stay valid here until the end of
	 * the loop.
	 */
	_unref_(sd_bus) *bus = sd_bus_ref(portal->bus);

	int rc;
	do {
		rc = sd_bus_process(bus, NULL);
	} while (rc > 0);

	if (rc != 0) {
		log_error(ei_portal_parent(portal), "dbus processing failed with %s", strerror(-rc));
	}
}

static int
portal_init(struct ei *ei, const char *busname)
{
	_cleanup_(sd_bus_error_free) sd_bus_error error = SD_BUS_ERROR_NULL;
	_unref_(sd_bus_message) *m = NULL;
	_unref_(sd_bus_message) *response = NULL;
	_unref_(sd_bus) *bus = NULL;
	_unref_(sd_bus_slot) *slot = NULL;
	_unref_(ei_portal) *portal = ei_portal_create(&ei->object);
	const char *path = NULL;

	int rc = sd_bus_open_user(&bus);
	if (rc < 0) {
		log_error(ei, "Failed to init dbus: %s\n", strerror(-rc));
		return -ECONNREFUSED;
	}

	_cleanup_free_ char *token = NULL;
	_cleanup_free_ char *handle = xdp_token(bus, &token);

	rc = sd_bus_match_signal(bus, &slot,
				 busname,
				 handle,
				 "org.freedesktop.portal.Request",
				 "Response",
				 portal_response_received,
				 portal);
	if (rc < 0) {
		log_error(ei, "Failed to subscribe to signal: %s\n", strerror(-rc));
		return -ECONNREFUSED;
	}

	rc = sd_bus_call_method(bus,
				busname,
				"/org/freedesktop/portal/desktop",
				"org.freedesktop.portal.EmulatedInput",
				"EmulateInput",
				&error,
				&response,
				"a{sv}", 1,
				"handle_token", /* string key */
				"s", token /* variant string */
			       );

	if (rc < 0) {
		log_error(ei, "Failed to call method: %s\n", strerror(-rc));
		return -ECONNREFUSED;
	}

	rc = sd_bus_message_read(response, "o", &path);
	if (rc < 0) {
		log_error(ei, "Failed to parse response: %s\n", strerror(-rc));
		return -ECONNREFUSED;
	}

	log_debug(ei, "portal Response object is %s\n", path);

	struct source *s = source_new(sd_bus_get_fd(bus), dbus_dispatch, portal);
	source_never_close_fd(s); /* the bus object handles the fd */
	rc = sink_add_source(ei->sink, s);
	if (rc == 0) {
		portal->bus_source = source_ref(s);
		portal->bus = sd_bus_ref(bus);
		portal->slot = sd_bus_slot_ref(slot);
	}

	portal->busname = xstrdup(busname);

	ei->backend = ei_portal_ref(portal);
	ei->backend_interface = interface;

	source_unref(s);

	return 0;
}

_public_ int
ei_setup_backend_portal(struct ei *ei)
{
	return portal_init(ei, "org.freedesktop.portal.Desktop");
}

_public_ int
ei_setup_backend_portal_busname(struct ei *ei, const char *busname)
{
	return portal_init(ei, busname);
}
