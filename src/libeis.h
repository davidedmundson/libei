/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */


#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

/**
 * @addtogroup libeis EIS - The server API
 *
 * libeis is the server-side module. This API should be used by processes
 * that have control over input devices, e.g. Wayland compositors.
 *
 * @{
 */

struct eis;
struct eis_client;
struct eis_device;
struct eis_seat;
struct eis_event;
struct eis_keymap;

enum eis_device_capability {
	EIS_DEVICE_CAP_POINTER = 1,
	EIS_DEVICE_CAP_POINTER_ABSOLUTE,
	EIS_DEVICE_CAP_KEYBOARD,
	EIS_DEVICE_CAP_TOUCH,
};

enum eis_keymap_type {
	EIS_KEYMAP_TYPE_XKB = 1,
};

enum eis_event_type {
	/**
	 * A client has connected. This is the first event from any new
	 * client.
	 * The server is expected to either call eis_event_client_allow() or
	 * eis_event_client_deny().
	 */
	EIS_EVENT_CLIENT_CONNECT = 1,
	/**
	 * The client has disconnected, any pending requests for this client
	 * should be discarded.
	 */
	EIS_EVENT_CLIENT_DISCONNECT,

	/**
	 * The client requests creation of a device with a given set of
	 * capabilities. A client may create more than one device and more
	 * than one device with the same capabilities. The server may filter
	 * the capabilities or deny them altogether.
	 *
	 */
	EIS_EVENT_DEVICE_ADDED,
	/**
	 * The device created by the client was removed. The server must
	 * call eis_device_disconnect() in response to this device.
	 *
         * libeis guarantees this event is generated before
	 * @ref EIS_EVENT_CLIENT_DISCONNECT.
	 */
	EIS_EVENT_DEVICE_REMOVED,

	EIS_EVENT_POINTER_MOTION = 300,
	EIS_EVENT_POINTER_MOTION_ABSOLUTE,
	EIS_EVENT_POINTER_BUTTON,
	EIS_EVENT_POINTER_SCROLL,
	EIS_EVENT_POINTER_SCROLL_DISCRETE,

	EIS_EVENT_KEYBOARD_KEY = 400,

	EIS_EVENT_TOUCH_DOWN = 500,
	EIS_EVENT_TOUCH_UP,
	EIS_EVENT_TOUCH_MOTION,
};

/**
 * Create a new libeis context with a refcount of 1.
 */
struct eis *
eis_new(void *user_data);

enum eis_log_priority {
	EIS_LOG_PRIORITY_DEBUG = 10,
	EIS_LOG_PRIORITY_INFO = 20,
	EIS_LOG_PRIORITY_WARNING = 30,
	EIS_LOG_PRIORITY_ERROR = 40,
};

/**
 * The log handler for library logging. This handler is only called for
 * messages with a log level equal or greater than than the one set in
 * eis_log_set_priority().
 *
 * @param message The log message as a null-terminated string
 * @param is_continuation The message is a continuation of the previous
 * message. The caller should skip any per-line-based prefixes.
 */
typedef void (*eis_log_handler)(struct eis *eis,
				enum eis_log_priority priority,
				const char *message,
				bool is_continuation);
/**
 * Change the log handler for this context. If the log handler is NULL, the
 * built-in default log function is used.
 *
 * @param eis The EIS context
 * @param log_handler The log handler or NULL to use the default log
 * handler.
 */
void
eis_log_set_handler(struct eis *eis, eis_log_handler log_handler);

void
eis_log_set_priority(struct eis *eis, enum eis_log_priority priority);

enum eis_log_priority
eis_log_get_priority(const struct eis *eis);

struct eis *
eis_ref(struct eis *eis);

struct eis *
eis_unref(struct eis *eis);

void *
eis_get_user_data(struct eis *eis);

void
eis_set_user_data(struct eis *eis, void *user_data);

/**
 * Initialize the context with a UNIX socket name.
 * If the path does not start with / it is relative to $XDG_RUNTIME_DIR.
 */
int
eis_setup_backend_socket(struct eis *ctx, const char *path);

/**
 * Initialize the context that can take pre-configured sockets.
 */
int
eis_setup_backend_fd(struct eis *ctx);

/**
 * Add a new client to a context set up with eis_setup_backend_fd()
 */
int
eis_backend_fd_add_fd(struct eis *ctx, int fd);

/**
 * libeis keeps a single file descriptor for all events. This fd should be
 * monitored for events by the caller's mainloop, e.g. using select(). When
 * events are available on this fd, call libeis_dispatch() immediately to
 * process.
 */
int
eis_get_fd(struct eis *eis);

/**
 * Main event dispatching function. Reads events of the file descriptors
 * and processes them internally. Use libeis_get_event() to retrieve the
 * events.
 *
 * Dispatching does not necessarily queue events. This function
 * should be called immediately once data is available on the file
 * descriptor returned by libeis_get_fd().
 */
void
eis_dispatch(struct eis *eis);

/**
 * Returns the next event in the internal event queue (or NULL) and removes
 * it from the queue.
 *
 * The returned event is refcounted, use eis_event_unref() to drop the
 * reference.
 *
 * You must not call this function while holding a reference to an event
 * returned by eis_peek_event().
 */
struct eis_event *
eis_get_event(struct eis *eis);

/**
 * Returns the next event in the internal event queue (or NULL) without
 * removing that event from the queue, i.e. the next call to eis_get_event()
 * will return that same event.
 *
 * This call is useful for checking whether there is an event and/or what
 * type of event it is.
 *
 * Repeated calls to eis_peek_event() return the same event.
 *
 * The returned event is refcounted, use eis_event_unref() to drop the
 * reference.
 *
 * A caller must not call eis_get_event() while holding a ref to an event
 * returned by eis_peek_event().
 */
struct eis_event *
eis_peek_event(struct eis *eis);

/**
 * Release resources associated with this event. This function always
 * returns NULL.
 *
 * The caller cannot increase the refcount of an event. Events should be
 * considered transient data and not be held longer than required.
 * eis_event_unref() is provided for consistency (as opposed to, say,
 * eis_event_free()).
 */
struct eis_event *
eis_event_unref(struct eis_event *event);

struct eis_client *
eis_client_ref(struct eis_client *client);

struct eis_client *
eis_client_unref(struct eis_client *client);

void *
eis_client_get_user_data(struct eis_client *eis_client);

void
eis_client_set_user_data(struct eis_client *eis_client, void *user_data);

/**
 * Return the name set by this client. The server is under no obligation to
 * use this name.
 */
const char *
eis_client_get_name(struct eis_client *client);

/**
 * Allow connection from the client. This can only be done once, further
 * calls to this functions are ignored.
 *
 * When receiving an event of type @ref EIS_EVENT_CLIENT_CONNECT, the server
 * should connect client as soon as possible to allow communication with the
 * server. If the client is not authorized to talk to the server, call
 * eis_client_disconnect().
 */
void
eis_client_connect(struct eis_client *client);

/**
 * Disconnect this client. Once disconnected the client may no longer talk
 * to this context, all resources associated with this client should be
 * released.
 *
 * It is not necessary to call this function when an @ref
 * EIS_EVENT_CLIENT_DISCONNECT event is received.
 */
void
eis_client_disconnect(struct eis_client *client);

/**
 * Create a new logical seat with a given name. Devices created by the
 * client must bind to a seat, or in other words: a client cannot create
 * devices until at least one seat is bound.
 *
 * This seat is not immediately active, use eis_seat_add() to bind this
 * seat on the client and notify the client of it's availability.
 *
 * The returned seat is refcounted, use eis_seat_unref() to drop the
 * reference.
 */
struct eis_seat *
eis_client_new_seat(struct eis_client *client, const char *name);

struct eis_seat *
eis_seat_ref(struct eis_seat *seat);

struct eis_seat *
eis_seat_unref(struct eis_seat *seat);

struct eis_client *
eis_seat_get_client(struct eis_seat *eis_seat);

const char *
eis_seat_get_name(struct eis_seat *eis_seat);

void *
eis_seat_get_user_data(struct eis_seat *eis_seat);

void
eis_seat_set_user_data(struct eis_seat *eis_seat, void *user_data);

/**
 * Allow a capability on the seat. This indicates to the client
 * that it may create devices with with the given capabilities, though the
 * EIS implementation may restrict the of capabilities on a device to a
 * subset of those in the seat, see eis_device_allow_capability().
 *
 * This function must be called before eis_seat_add().
 *
 * This function has no effect if called after eis_seat_add()
 */
void
eis_seat_allow_capability(struct eis_seat *seat,
			  enum eis_device_capability cap);

/**
 * Add this seat to its client and notify the client of the seat's
 * availability. This allows the client to create a device within this seat.
 */
void
eis_seat_add(struct eis_seat *seat);

/**
 * Remove this seat and all its remaining devices.
 */
void
eis_seat_remove(struct eis_seat *seat);

enum eis_event_type
eis_event_get_type(struct eis_event *event);

struct eis_client *
eis_event_get_client(struct eis_event *event);

struct eis_client *
eis_device_get_client(struct eis_device *device);

struct eis_seat *
eis_device_get_seat(struct eis_device *device);

struct eis_device *
eis_device_ref(struct eis_device *device);

struct eis_device *
eis_device_unref(struct eis_device *device);

void *
eis_device_get_user_data(struct eis_device *eis_device);

void
eis_device_set_user_data(struct eis_device *eis_device, void *user_data);

/**
 * Return the name of the device. The return value of this function may change after
 * eis_device_set_name(), a caller should keep a copy of it where required rather than the
 * pointer value.
 */
const char *
eis_device_get_name(struct eis_device *device);

/**
 * Set the name of the device. This function has no effect if called after
 * eis_device_connect()
 */
void
eis_device_set_name(struct eis_device *device, const char *name);

bool
eis_device_has_capability(struct eis_device *device,
			  enum eis_device_capability cap);

/**
 * Allow a capability on the device. This function must be called in
 * response to an @ref EIS_EVENT_DEVICE_ADDED and before
 * eis_device_connect().
 *
 * This function has no effect if called for a capability the device does
 * not support. This function has no effect if called for a capability not
 * allowed on the seat this device belongs to.
 *
 * This function has no effect if called after eis_device_connect()
 */
void
eis_device_allow_capability(struct eis_device *device,
			    enum eis_device_capability cap);

/**
 * Connects the device.
 *
 * This function should be called in response to an @ref
 * EIS_EVENT_DEVICE_ADDED if the server accepts the device creation.
 * Any changes to the device, e.g. eis_device_set_name() and
 * eis_device_disable_capability() must be performed before connecting the
 * device.
 *
 * Calling eis_device_connect() on a device with all capabilities set to
 * zero is a bug.
 *
 * If the device is rejected, call eis_device_disconnect() instead.
 *
 * The device is suspended, use eis_device_resume() to enable events from
 * the client.
 */
void
eis_device_connect(struct eis_device *device);

/**
 * Disconnect the device.
 * This does not release any resources associated with this device, use
 * eis_device_unref() for any references held by the caller.
 */
void
eis_device_disconnect(struct eis_device *device);

/**
 * Notify the client that the device is suspended and that no events
 * from the client will be processed.
 *
 * The library filters events sent by the client **after** the suspend
 * notification has been processed by the client but this does not affect
 * events already in transit. In other words, the server may still receive
 * a number of events from a device after it has been suspended and must
 * update its internal state accordingly.
 *
 * @param device A connected device
 */
void
eis_device_suspend(struct eis_device *device);

/**
 * Notify the client that the capabilities are resumed and that events
 * from the device will be processed.
 *
 * @param device A connected device
 */
void
eis_device_resume(struct eis_device *device);

/**
 * Get the width of the absolute pointer device in logical
 * pixels. The allowable range for absolute pointer motion is
 * [0, max) for each axis, i.e. zero inclusive, max exclusive. Coordinates
 * outside this range may be discarded or clipped silently by the library.
 *
 * The pointer range is constant. Where the pointer range is no longer
 * applicable, the client needs to remove the device and create and add a
 * new device with the updated pointer range.
 *
 * The server may use this in mapping heuristics. For example, a pointer
 * device with a pixel range of 1920x1200 **may** be automatically mapped by
 * the server to the monitor with this range, or a pointer device with a
 * ratio of R **may** be mapped to the monitor with the same ratio. This is
 * not a guarantee, the mapping policy is a private implementation detail
 * in the server. It is assumed that the client has other communication
 * channels (e.g. Wayland) to obtain the pointer range it needs to emulate
 * input on a device and channels to notify the server of desired mappings
 * (e.g. gsettings).
 *
 * It is a client bug to send pointer values outside this range.
 *
 * It is a server bug to call this function on a device without the @ref
 * EIS_DEVICE_CAP_POINTER_ABSOLUTE capability.
 *
 * @return The new width in logical pixels
 */
uint32_t
eis_device_pointer_get_width(struct eis_device *device);

/**
 * @see eis_device_pointer_get_width
 */
uint32_t
eis_device_pointer_get_height(struct eis_device *device);

/**
 * @see eis_device_pointer_get_width
 */
uint32_t
eis_device_touch_get_width(struct eis_device *device);

/**
 * @param device The EI device
 *
 * @see eis_device_touch_get_width
 */
uint32_t
eis_device_touch_get_height(struct eis_device *device);

/**
 * Create a new keymap of the given @p type. This keymap does not immediately
 * apply to the device, use eis_device_keyboard_set_keymap() to apply
 * this keymap.  A keymap may only be applied once and to a single device.
 *
 * The returned keymap has a refcount of at least 1, use eis_keymap_unref()
 * to release resources associated with this keymap.
 *
 * @param type The type of the keymap.
 * @param fd A memmap-able file descriptor of size @p size pointing to the
 * keymap used by this device. @p fd  can be closed by the caller after this
 * function completes.
 * @param size The size of the data at @p fd in bytes
 *
 * @return A keymap object or `NULL` on failure.
 */
struct eis_keymap *
eis_keymap_new(enum eis_keymap_type type, int fd, size_t size);

/**
 * @return the size of the keymap in bytes
 */
size_t
eis_keymap_get_size(struct eis_keymap *keymap);

/**
 * Returns the type for this keymap. The type specifies how to interpret the
 * data at the file descriptor returned by eis_keymap_get_fd().
 */
enum eis_keymap_type
eis_keymap_get_type(struct eis_keymap *keymap);

/**
 * Return a memmap-able file descriptor pointing to the keymap used by the
 * device. The keymap is constant for the lifetime of the device and
 * assigned to this device individually.
 */
int
eis_keymap_get_fd(struct eis_keymap *keymap);

struct eis_keymap *
eis_keymap_ref(struct eis_keymap *keymap);

struct eis_keymap *
eis_keymap_unref(struct eis_keymap *keymap);

/**
 * Return the device this keymap belongs to, or `NULL` if it has not yet
 * been assigned to a device.
 *
 * If the keymap is a client-assigned keymap and the server has changed or
 * unset the keymap with eis_device_keyboard_set_keymap(), this function
 * returns `NULL`.
 *
 */
struct eis_device *
eis_keymap_get_device(struct eis_keymap *keymap);

/**
 * Return the keymap assigned to this device. The return value of this
 * function is the client-assigned keymap (if any) before the call to
 * eis_device_keyboard_set_keymap(), or the server-assigned one after.
 */
struct eis_keymap *
eis_device_keyboard_get_keymap(struct eis_device *device);

/**
 * Set the keymap on the device. This overwrites the client's keymap
 * choice. Note that **not** calling this function when
 * eis_device_keyboard_get_keymap() returns non-`NULL` is equivalent to
 * accepting the client's choice of keymap.
 *
 * If the keymap is not `NULL`, the keymap is the one used by this device.
 * This keymap is constant for the lifetime of the device and assigned to
 * this device individually. Where the keymap has to change, remove the
 * device and wait for the client to create a new one.
 *
 * If the keymap is `NULL`, the device does not have an individual keymap
 * assigned. A server that does not handle individual client keymaps must
 * call this function with an @p keymap of `NULL`.
 *
 * This function has no effect if called after eis_device_connect()
 */
void
eis_device_keyboard_set_keymap(struct eis_device *device,
			       struct eis_keymap *keymap);

/**
 * Return the device from this event.
 *
 * This does not increase the refcount of the device. Use eis_device_ref()
 * to keep a reference beyond the immediate scope.
 */
struct eis_device *
eis_event_get_device(struct eis_event *event);

/**
 * For an event of type @ref EIS_EVENT_POINTER_MOTION return the relative x
 * movement in logical pixels.
 */
double
eis_event_pointer_get_dx(struct eis_event *event);

/**
 * For an event of type @ref EIS_EVENT_POINTER_MOTION return the relative y
 * movement in logical pixels.
 */
double
eis_event_pointer_get_dy(struct eis_event *event);

/**
 * For an event of type @ref EIS_EVENT_POINTER_MOTION_ABSOLUTE return the x
 * position in logical pixels.
 */
double
eis_event_pointer_get_absolute_x(struct eis_event *event);

/**
 * For an event of type @ref EIS_EVENT_POINTER_MOTION_ABSOLUTE return the y
 * position in logical pixels.
 */
double
eis_event_pointer_get_absolute_y(struct eis_event *event);

/**
 * For an event of type @ref EIS_EVENT_POINTER_BUTTON return the button
 * code as defined in linux/input-event-codes.h
 */
uint32_t
eis_event_pointer_get_button(struct eis_event *event);

/**
 * For an event of type @ref EIS_EVENT_POINTER_BUTTON return true if the
 * event is a button press, false for a release.
 */
bool
eis_event_pointer_get_button_is_press(struct eis_event *event);

/**
 * For an event of type @ref EIS_EVENT_POINTER_SCROLL return the x scroll
 * distance in logical pixels.
 */
double
eis_event_pointer_get_scroll_x(struct eis_event *event);

/**
 * For an event of type @ref EIS_EVENT_POINTER_SCROLL return the y scroll
 * distance in logical pixels.
 */
double
eis_event_pointer_get_scroll_y(struct eis_event *event);

/**
 * For an event of type @ref EIS_EVENT_POINTER_SCROLL_DISCRETE return the x
 * scroll distance in fractions or multiples of 120.
 */
int32_t
eis_event_pointer_get_scroll_discrete_x(struct eis_event *event);

/**
 * For an event of type @ref EIS_EVENT_POINTER_SCROLL_DISCRETE return the y
 * scroll distance in fractions or multiples of 120.
 */
int32_t
eis_event_pointer_get_scroll_discrete_y(struct eis_event *event);

/**
 * For an event of type @ref EIS_EVENT_KEYBOARD_KEY return the key code (as
 * defined in include/linux/input-event-codes.h).
 */
uint32_t
eis_event_keyboard_get_key(struct eis_event *event);

/**
 * For an event of type @ref EIS_EVENT_KEYBOARD_KEY return true if the
 * event is a key down, false for a release.
 */
bool
eis_event_keyboard_get_key_is_press(struct eis_event *event);

/**
 * For an event of type @ref EIS_EVENT_TOUCH_DOWN, @ref
 * EIS_EVENT_TOUCH_MOTION, or @ref EIS_EVENT_TOUCH_UP, return the tracking
 * ID of the touch.
 *
 * The tracking ID is a unique identifier for a touch and is valid from
 * touch down through to touch up but may be re-used in the future.
 * The tracking ID is randomly assigned to a touch, a client
 * must not expect any specific value.
 */
uint32_t
eis_event_touch_get_id(struct eis_event *event);

/**
 * For an event of type @ref EIS_EVENT_TOUCH_DOWN, or @ref
 * EIS_EVENT_TOUCH_MOTION, return the x coordinate of the touch.
 */
double
eis_event_touch_get_x(struct eis_event *event);

/**
 * For an event of type @ref EIS_EVENT_TOUCH_DOWN, or @ref
 * EIS_EVENT_TOUCH_MOTION, return the y coordinate of the touch.
 */
double
eis_event_touch_get_y(struct eis_event *event);

/**
 * @}
 */

#ifdef __cplusplus
}
#endif
